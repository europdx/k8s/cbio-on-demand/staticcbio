# Deploy cBioPortal to K8s cluster


Clone repository. Make sure static TMPlist id is set correctly in cbio-replicaset.yml.

Start deployment with

`kubectl create -n cbio-on-demand -f .`

Check the results with following.

`kubectl get all -n cbio-on-demand -l type=static`

`watch kubectl get all -n cbio-on-demand -l type=static`


# Delete static cBioPortal

Static deployment can be deleted with following.

`kubectl delete all -n cbio-on-demand -l type=static`
